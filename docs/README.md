Depend\Depend
=============

Dependency Injection in PHP involves creating instances that might, or might
not be actually used. Whenever an instance of a class is created, it has to 
declare in advance which services it will be using, by either type hinting in
the constructor, or by using comment annotations on properties.

Both these techniques work, but they also cause database connections and
other objects to be created in advance.


Trait based dependency injection
--------------------------------

Depend implements trait based dependency injection. If you want to be able
to use a PDO connection, or a Psr\Log\LoggerInterface compatible service,
you simply declare them like this:

    <?php
    namespace MyLibrary;

    class MyClass {

        /**
         * Dependencies:
         */
        use PDO {
            PDO as db;
        }

        use Psr\Log\LoggerInterface {
            LoggerInterface as log;
        }


        /**
         * Using a dependency
         */
        public function doSomeLogging() {
            $this->log()->info("Hello");
        }      


    }
    ?>


Portable libraries
------------------

We recommend that all libraries activate Depend for their namespace, by calling

    <?php
    namespace MyLibrary;

    new Depend(null, __NAMESPACE__);
    ?>

If you wish to use Dependency Injection internally in your library, regardless
of any existing service container you can do:

    <?php
    namespace MyLibrary;

    new Depend( $anyPsr11ServiceContainer, __NAMESPACE__ );
    ?>


If your library requires a global service container to work, you can declare this
by calling:

    <?php
    namespace MyLibrary;

    Depend::required();
    ?>

This will cause an exception if your library is being hosted in an environment
where Depend has not already been configured.

In this case, the developer would only have to do:

    <?php
    Depend::setDefaultContainer( $container );
    ?>













Frameworks without native support
---------------------------------

Depend was designed to be portable. If your library is being used in an environment
where dependency injection is used, you can safely 

    <?php
    namespace MyLibrary;

    use Depend\Depend;
    use League\Container\Container;

    $defaultContainer = new Container();

    // If you have services

    new Depend( $defaultContainer, __NAMESPACE__ );
    ?>






Integrating with frameworks that don't natively support Depend-style
automatic injection is simple:

    <?php
    /**
     * Make sure there is a default service container available.
     */
    Depend::setDefaultContainer( $container );
    ?>

Any PSR-11 compatible service container will work out of the box.






Enabling native support in your framework
-----------------------------------------

If you want to enable Depend-style dependency injection in your
framework, you simply have to call Depend::setDefaultContainer() with
your main dependency injection controller.


Importing dependencies into your namespace
------------------------------------------

If you want dependencies imported into your namespace, you can do
this in t ways:

Bring your own service container:

    <?php
    namespace MyLibrary;

    new Depend( $myServiceContainer, __NAMESPACE__ );
    ?>

or, by going directly to the top level dependencies - which are 
made available in a special namespace called 'services':

    <?php
    class YourClass {
        use \service\Psr\Log\LoggerInterface { 
            LoggerInterface as log; 
        }
    }








The only thing your library needs to do, to be able to use this method
for dependency injection, is to access access a PSR-11 compatible
Service Container and for each namespace you want dependency injection
enabled, you do:

    <?php
    /**
     * If your library is being used in a framework that does not
     * natively support Depend style injections, you can easily
     * register the 


    Depend::setDefaultContainer( $container );


    // Your top level container
    new Depend($container, 'MyLibrary');

    // Nested containers
    new Depend(null, 'MyLibrary\Controllers', 'MyLibrary');
    new Depend(null, 'MyLibrary\Utils', 'MyLibrary');
    ?>


Framework Support
-----------------

If you maintain a framework, or wish to enable this type of 





Benefits
--------

* 



Importing a Service
-------------------

    <?php
    namespace MyLibrary;

    class MyLibrary {

        /**
         * Declare which dependencies your class has by using a special
         * trait:
         */
        use Psr\Log\LoggerInterface {
            LoggerInterface as log;
        }

        /**
         * Now you can use the log() method:
         */
        /* protected function log(): LoggerInterface; */
    }


Namespaces
----------

Depend creates traits *inside your namespace*

Services are imported into your namespace, simply by creating an instance of
Depend.

Dependencies are resolved hierarchically; If a dependency can't be found in
your namespace, Depend will check the parent namespace until it has resolved
your dependency - or it fails by throwing Psr\Container\NotFoundExceptionInterface 

Most PHP applications are built using a framework, and this framework should
register its main service container by calling:

    new Depend($container);

This will create a special namespace for PHP, called 'service'. Dependencies
can be imported from 'service' like this:

    <?php
    namespace MyNamespace;

    class MyClass {
        use \service\Psr\Log\LoggerInterface {
            LoggerInterface as log;
        }
    }
    ?>



Setup for frameworks
--------------------

Frameworks are supposed to provide the top level services, 


    <?php
    namespace MyLibrary;

    $container = new \League\Container\Container();
 
    // ... register some services to the container

    new \Depend\Depend($container, __NAMESPACE__);


Importing Dependencies
----------------------

    <?php
    namespace MyLibrary;

    class {

        /**
         * Notice that Psr\Log\LoggerInterface actually refers to a
         * trait named MyLibrary\Psr\Log\LoggerInterface
         */
        use Psr\Log\LoggerInterface {
            LoggerInterface as log;
        }

        public function testTheLogger(): voide {
            $this->log()->info("Yes! I have a logger interface!");
        }
    }


How it works
------------







Depend tries to solve an annoying problem with PHP Dependency Injection, by
utilizing already existing functionality in the PHP language: Traits.

Depend will automatically create the source code for your traits and store 
them to disk, allowing PHP opcode caching and highest possible performance.

Also, your library be able to use dependency injection in environments
where dependency injection is not used, via Namespaces.


    <?php

    class YourClass {

        // Add this:
        
        use Depend\Psr\Logger\LoggerInterface {
            LoggerInterface as log;
        }

        // Get this:

        protected function log(): \Psr\Logger\LoggerInterface;
    }
    ?>


Integrating in frameworks
-------------------------

If your framework has a PSR-11 compatible library, you can simply register it
like this:

    <?php
    new Depend\Depend($serviceContainer);
    ?>

This registers the namespace \Depend\ for all services provided by your
service container.
 

Libraries/Composer Packages
---------------------------

If your library uses dependency injection internally, you can easily 
register a dedicated namespace:

    <?php
    new Depend\Depend($yourOwnServiceContainer, 'YourNamespace');
    ?>

Your code will then use \YourNamespace for resolving dependencies.
Example:

    <?php
    class YourClass {
        use YourNamespace\Psr\Logger\LoggerInterface {
            LoggerInterface as log;
        }
    }
    ?>






Libraries can select one of two approaches for using Depend:

1. Adopt any compliant service container into their own namespace:

    <?php
    new Depend\Depend($whateverServiceContainer, 'MyNameSpace');

    

1. Bring your own service container


Libraries should register a namespace for their dependencies,

Libraries can use Depend in two ways; run their own namespaced instance which fallbacks
to the main instance if it exists, or register 

Libraries should have their own instance of Depend. This way, your library will not break
if

    <?php
    use Depend\Depend;
    use League\Container\Container;

    // Create a new container, or use an existing container
    $ourContainer = new Container();
    $ourContainer->add(Psr\Logger\LoggerInterface::class, $someInstance);
    
    $ourInjector = new Depend($ourContainer, "MyNamespace\\");

    /**
     * You can now request instances this way:
     *
     * class MyClass {
     *    use MyNamespace\Psr\Logger\LoggerInterface {
     *        use LoggerInterface as log;
     *    }
     * }
     */
    ?>

The instances will fallback to 


Namespaces
----------

Your library can easily register your own dependency namespaces by creating your own
instance of Depend:

    $myDependInstance = new \Depend\Depend(new ServiceContainer(), 'MyNamespace\\');


In your classes you would now register dependencies like this:

    class YourClass {
        use MyNamespace\\Psr\Logger\LoggerInterface {
            LoggerInterface as log;
        }

        // ...
    }


Fallback Namespace
------------------

Depend will automatically fallback to the Depend\\ namespace, if your service 


Advantages
----------

Depend takes the nature of PHP into account. Since everything is discarded after the
job is done - we should never create instances of services that are not used.

With Depend, we will only instantiate the dependency if it is needed. Since PHP
is not Java, this is a great benefit. Java is a long running program where a single
injected dependency will be alive for a very long time - while PHP is usually
garbage collected immediately after the response has been sent.

At the same time:

* Inversion of Control is taken care of: We're using the 'Service Locator Pattern'
  via the provided PSR-11 compatible container. At the same time, the disadvantages
  that Wikipedia mentions of the Service Locator Pattern is taken care of:

  - We don't hide the class dependencies; they are declared in the class definition.
  - Run-time errors is mostly a problem for long-running processes like you would
    have in Java.
  - The difficulty of maintenance that is mentioned applies to Dependency Injection
    regardless of methodology.
  - The code is easy to test, because you can easily use a different service locator
    in your unit testing, by simply calling $depend = new Depend($testContainer);


Disadvantages
--------------

With Depend, it becomes harder to provide different instances to different services.

I'm not sure if this is a plausible scenario. I can't recall needing it in the soon
to be 20 years I've been a professional PHP developer.

If this



Background
==========

Inversion of Control is a term invented in 2004 by Martin Fowler. The basic
idea is that functionality should not have to worry about how and where 
services such as database, logging and other important aspects of an application
comes into existence.

There are many ways to achieve this inversion of control, and Dependency Injection
is one of them.

There are also many ways to "inject dependencies" into a class. For example, in
the Spring Framework (Java) you have two primary methods:

  * Construction Injection
  * Setter Injection

Construction Injection
----------------------

This appears to be the ubiquitous methodology used in the PHP community. You
simply declare which interfaces you need in the class constructor, and then
instances that implement the requested interface is passed into the constructor
of your class.

Example:

    <?php
    class MyClass {

        protected $db;

        public function __construct(\PDO $db) {
            $this->db = $db;
        }

    }
    ?>

Setter Injection
----------------

This is a different methodology, which is also possible in PHP. This method
can be implemented if the "constructing" code uses reflection and detects
which properties and types the class has. In PHP this will look something 
like this:

    <?php
    class MyClass {

        /**
         * PHP 7.4 will support typed properties, so we only need to annotate
         * with the Inject keyword.
         *
         * @Inject
         */
        protected Something1 $something1;

        /**
         * Another method commonly supported is very similar, but uses a 
         * setter method:
         *
         * @Inject
         */
        public function setSomething2(Something2 $something2): void {
            $this->something2 = $something2;
        }

        /**
         * Pre PHP 7.4, you would specify the interface name like this
         *
         * @Inject("Something3")
         */
        protected $something3;
    }
    ?>

All of these implementations basically perform the same action. Injection could
also be declared in configuration files such as the config/services.yaml file
of Symfony.


