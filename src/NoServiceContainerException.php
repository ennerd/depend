<?php
/**
 * Exception thrown when the namespace you are accessing
 * does not have a service container and no parent
 * namespace
 *
 * @package   depend/depend
 * @link      https://bitbucket.org/ennerd/depend
 * @author    Frode Børli <frode@ennerd.com>
 * @copyright 2019 Frode Børli
 * @license   https://opensource.org/licenses/MIT MIT License
 */
declare(strict_types=1);
namespace Depend;

use Psr\Container\NotFoundExceptionInterface;

class NoServiceContainerException extends Exception implements NotFoundExceptionInterface {
    public function __construct(string $namespace) {
        parent::__construct("There is no Depend service container registered for '$namespace'. Did you call '\Depend\Depend::registerContainer(".var_export($namespace, true).", \$container);'?");
    }
}
