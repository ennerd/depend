<?php
/**
 * Parent exception for all container exceptions emitted by Depend
 *
 * @package   depend/depend
 * @link      https://bitbucket.org/ennerd/depend
 * @author    Frode Børli <frode@ennerd.com>
 * @copyright 2019 Frode Børli
 * @license   https://opensource.org/licenses/MIT MIT License
 */
declare(strict_types=1);
namespace Depend;

use Psr\Container\ContainerExceptionInterface;

class ContainerException extends Exception implements ContainerExceptionInterface
{
}
