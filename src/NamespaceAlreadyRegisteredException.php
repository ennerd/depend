<?php
/**
 * Exception thrown when registering the root namespace twice, and
 * the new container is different from the previous namespace.
 *
 * This indicates disagreement about what is the root namespace,
 * and different components should never insert the wrong 
 * container in the root namespace.
 *
 * This behavior may change in the future, if multiple containers
 * become allowed on a single namespace.
 *
 * @package   depend/depend
 * @link      https://bitbucket.org/ennerd/depend
 * @author    Frode Børli <frode@ennerd.com>
 * @copyright 2019 Frode Børli
 * @license   https://opensource.org/licenses/MIT MIT License
 */
declare(strict_types=1);
namespace Depend;

class NamespaceAlreadyRegisteredException extends Exception {
}
