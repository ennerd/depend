<?php
/**
 * Exception thrown when user tries to declare an illegal namespace name
 *
 * @package   depend/depend
 * @link      https://bitbucket.org/ennerd/depend
 * @author    Frode Børli <frode@ennerd.com>
 * @copyright 2019 Frode Børli
 * @license   https://opensource.org/licenses/MIT MIT License
 */
declare(strict_types=1);
namespace Depend;

class InvalidNamespaceException extends Exception
{
}
