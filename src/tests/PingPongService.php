<?php
namespace Depend\Tests;

class PingPongService
{

    public function ping(): string
    {
        return "pong";
    }
}
