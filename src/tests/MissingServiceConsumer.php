<?php
namespace Depend\Tests;

class MissingServiceConsumer
{

    use \service\MissingService {
        MissingService as missing;
    }

    public function getMissingService()
    {
        return $this->missing();
    }

    public function missingViaAlias()
    {
        return $this->missing()->missingMethod();
    }

    public function missingDirectly()
    {
        return $this->MissingService()->missingMethod();
    }
}
