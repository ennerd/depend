<?php
namespace Depend\Tests;

use Depend\Depend;
use League\Container\Container;

trait UnitTestTrait
{

    protected $depends = [];
    protected $containers = [];

    public function tearDown(): void
    {
        foreach ($this->depends as $depend) {
            $depend->unregister();
        }
    }

    public function makeDepends(array $config)
    {
        foreach ($config as $c) {
            $this->depends[] = new Depend($c[0], $c[1]);
        }
    }

    public function useDefault()
    {
        $this->makeDepends([ new Container(), Depend::DEFAULT_NAMESPACE ]);
    }

    public function useOther()
    {
        $this->makeDepends([ new Container(), 'Other' ]);
    }

    public function useBoth()
    {
        $this->useDefault();
        $this->useOther();
    }
}
