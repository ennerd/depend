<?php
namespace Depend\Tests;

class OtherPingPongConsumer
{

    use \Other\Depend\Tests\PingPongService {
        PingPongService as ping;
    }

    public function getPingService()
    {
        return $this->ping();
    }

    public function pingViaAlias()
    {
        return $this->ping()->ping();
    }

    public function pingDirectly()
    {
        return $this->PingPongService()->ping();
    }
}
