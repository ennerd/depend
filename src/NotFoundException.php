<?php
/**
 * Exception thrown when an interface is not found in the container.
 *
 * @package   depend/depend
 * @link      https://bitbucket.org/ennerd/depend
 * @author    Frode Børli <frode@ennerd.com>
 * @copyright 2019 Frode Børli
 * @license   https://opensource.org/licenses/MIT MIT License
 */
declare(strict_types=1);
namespace Depend;

use Psr\Container\NotFoundExceptionInterface;

class NotFoundException extends Exception implements NotFoundExceptionInterface
{
}
