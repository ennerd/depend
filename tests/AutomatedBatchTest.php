<?php
use PHPUnit\Framework\TestCase;
use Depend\Depend;

/**
 * Test behavior when there is no container globally or locally.
 */
class AutomatedBatchTest extends TestCase
{
    protected $depends = [];
    protected function tearDown(): void {
        foreach($this->depends as $depend) {
            $depend->unregister();
        }
        $this->depends = [];
    }

    public function testDefaultNamespace() {
        $this->depends[] = new Depend();
        $this->assertTrue(true);
    }
    public function testOtherNamespaceEmptyException() {
        $this->depends[] = new Depend('Other');
        $this->assertTrue(true);
    }

    public function testDoubleRegistration() {
        $this->expectException(\Depend\NamespaceAlreadyRegisteredException::class);
        $this->depends[] = new Depend('Other');
        $this->depends[] = new Depend('Other');
    }

    public function testServiceNotFoundException() {
        $this->depends[] = new Depend('Other');
        $this->expectException(\Psr\Container\NotFoundExceptionInterface::class);
        $consumer = new \Depend\Tests\OtherPingPongConsumer();
        $consumer->pingDirectly(); 
    }

    public function testServiceNotFoundException2() {
        $this->depends[] = new Depend('Other');
        $this->expectException(\Psr\Container\NotFoundExceptionInterface::class);
        $consumer = new \Depend\Tests\OtherPingPongConsumer();
        $consumer->pingViaAlias();
    }

    public function testServiceWrongNamespaceButMainNamespaceExists() {
        $mainContainer = new \League\Container\Container();
        $mainContainer->add(\Depend\Tests\PingPongService::class, new \Depend\Tests\PingPongService());

        Depend::setDefaultContainer( $mainContainer );
        $this->depends[] = new Depend();
        $this->expectException(\Depend\UnregisteredNamespaceException::class);
        $consumer = new \Depend\Tests\OtherPingPongConsumer();
        $consumer->pingViaAlias();
    }

    public function testValidNamespaces(): void {
        foreach(['Other', 'SomeThing', 'Some_Thing', "Nested\\Namespace", "Root\\Namespace"] as $namespace) {
            $depend = new Depend($namespace);
            $depend->unregister();
        }
        $this->assertTrue(true);
    }


    public function testInvalidNamespaces(): void {
        // The default namespace is not allowed to be empty
        foreach(["\\RootNamespace", "EndNamespace\\", "\\BothSides\\", "SpaceAtEnd ", " SpaceAtStart", 'Space is not ok', '#whatever', 'whatever#', ' SurroundingSpace ', '.', '', 'Dot.In.Namespace', '123StartsWithNumber'] as $namespace) {
            try {
                $depend = new Depend($namespace);
                $this->assertFalse(true, "Invalid namespace '$namespace' was accepted.");
                $depend->unregister();
            } catch (\Depend\InvalidNamespaceException $e) {
                $this->assertTrue(true);
            }
        }
    }

 //   public function testDefaultNamespace
 
/*
    public function testEverything(): void {

        $namespaces = ['Depend', 'Other'];
        $serviceProviders = ['PingPongService'];
        $missingServiceProviders = ['MissingService'];

        foreach($namespaces as $namespace) {
            $depend = new Depend(null, $namespace);
            $depend->unregister();
        }

    }
*/
}
